sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	'../model/Formatter',
	'sap/viz/ui5/data/FlattenedDataset',
	'sap/viz/ui5/format/ChartFormatter',
	'sap/viz/ui5/api/env/Format',
	'sap/ui/model/Filter',
	'sap/m/Dialog',
	'sap/m/Button',
	'sap/m/Text'

], function(Controller, JSONModel, Formatter, FlattenedDataset, ChartFormatter, Format, Filter, Dialog, Button, Text) {
	"use strict";

	return Controller.extend("academy-webapp.controller.MainView", {

		onInit: function() {
			this._employeeModel = new JSONModel();
			this._statisticModel = new JSONModel();
			this._employeeObj = new JSONModel();
			this.getView().setModel(this._employeeModel, "employees");
			this.getView().setModel(this._statisticModel, "statistic");
			this.getView().setModel(this._employeeObj, "employeeObject");
			var formatPattern = ChartFormatter.DefaultPattern;
			var oVizFrame = this.oVizFrame = this.getView().byId("idVizFrame");
			oVizFrame.setVizProperties({
				plotArea: {
					dataLabel: {
						visible: true
					}
				},
				valueAxis: {
					label: {},
					title: {
						visible: false
					}
				},
				categoryAxis: {
					title: {
						visible: false
					}
				},
				title: {
					visible: false,
					text: 'Number of Employees'
				}
			});
			oVizFrame.setModel(this._statisticModel);
			var oPopOver = this.getView().byId("idPopOver");
			oPopOver.connect(oVizFrame.getVizUid());

			// 	var oChartContainerContent = new sap.suite.ui.commons.ChartContainerContent({
			// 		icon: "sap-icon://line-chart",
			// 		title: "vizFrame Line Chart Sample",
			// 		content: [oVizFrame]
			// 	});
			// 	var oChartContainer = new sap.suite.ui.commons.ChartContainer({
			// 		content: [oChartContainerContent]
			// 	});
			// //	oChartContainer.setShowFullScreen(true);
			// //	oChartContainer.setAutoAdjustHeight(true);

			this.onGetEmployees();
			this.onGetStatistic();
		},

		onAfterRendering: function() {

		},

		onGetEmployees: function() {
			this.showBusyIndicator();
			jQuery.ajax({
				type: "GET",
				url: "https://backend-intelligent-emu.cfapps.eu10.hana.ondemand.com/api/v1/employees",
				success: function(data, textStatus, jqXHR) {
					var femaleNumber = 0;
					data.find(function(employee, index) {
						if (employee.gender === 'Female') {
							femaleNumber++;
						}
					});
					var model = {
						"employees": data,
						"total": data.length,
						"femalePercentage": (femaleNumber / data.length) * 100
					};
					this.employees = {
						"employees": data
					};
					this._employeeModel.setData(model);
					this.getView().byId("employees_id").setModel(this._employeeModel);
					this.getView().byId("totalEmployeeTile_id").setModel(this._employeeModel);
					this.getView().byId("statisticTile_id").setModel(this._employeeModel);
					this.hideBusyIndicator();

				}.bind(this),
				error: function(error) {
					this.hideBusyIndicator();
				}.bind(this)
			});
		},
		
		onGetEmployeeSelected: function(id) {
			this.showBusyIndicator();
			jQuery.ajax({
				type: "GET",
				url: "https://backend-intelligent-emu.cfapps.eu10.hana.ondemand.com/api/v1/employees/" + id,
				success: function(data, textStatus, jqXHR) {
					this._employeeObj.setData(data);
					var oHistoricDialog = this._getDialog();
					oHistoricDialog.open();
					this.hideBusyIndicator();

				}.bind(this),
				error: function(error) {
					this.hideBusyIndicator();
				}.bind(this)
			});
		},

		onGetStatistic: function() {

			jQuery.ajax({
				type: "GET",
				url: "https://backend-intelligent-emu.cfapps.eu10.hana.ondemand.com/api/v1/statistic",
				success: function(data, textStatus, jqXHR) {
					var model = {
						"statistic": data
					};

					this._statisticModel.setData(model);
					this.oVizFrame.setModel(this._statisticModel);

				}.bind(this),
				error: function(error) {

				}.bind(this)
			});
		},

		onListItemPressed: function(oEvent) {
			var list = this.getView().byId('employees_id');
			list.setSelectedItem('');
			var selectedEmail = oEvent.getSource().getSelectedItem().getDescription();
			this.employees.employees.find(function(e) {
				if (e.email === selectedEmail) {
					this.onGetEmployeeSelected(e.id);
				}
			}.bind(this));
			
		},

		_getDialog: function() {
			this.showBusyIndicator();
			if (this.personDialog) {
				this.hideBusyIndicator();
				return this.personDialog;
			}
			// associate controller with the fragment
			this.personDialog = sap.ui.xmlfragment("academy-webapp.view.popup", this);
			this.getView().addDependent(this.personDialog);

			//this.getView().byId("personPanel").setModel(this._employeeModel);
			sap.ui.getCore().byId("personPanel").setModel(this._employeeObj, "employeeObject");

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this.personDialog);

			var oOKButton = this.personDialog.getAggregation("buttons")[1];
			this.personDialog.setTitle('Employee Full');
			oOKButton.setVisible(false);
			this.bIsReseted = true;
			this.hideBusyIndicator();
			return this.personDialog;
		},

		handleOK: function(oEvent) {
			this.personDialog.close();
			
		},

		onSearch: function(oEvt) {

			// add filter for search
			var aFilters = [];
			var sQuery = oEvt.getSource().getValue();
			if (sQuery && sQuery.length > 0) {
				var filter = new Filter("lastName", sap.ui.model.FilterOperator.Contains, sQuery);
				aFilters.push(filter);
			}

			// update list binding
			var list = this.getView().byId("employees_id");
			var binding = list.getBinding("items");
			binding.filter(aFilters, "Application");
		},
		
		showBusyIndicator: function() {
			sap.ui.core.BusyIndicator.show(0);
		},
		hideBusyIndicator: function() {
			sap.ui.core.BusyIndicator.hide();
		}

	});
});