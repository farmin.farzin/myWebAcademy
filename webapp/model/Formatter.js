sap.ui.define(function() {
	"use strict";

	var Formatter = {

		genderStatus :  function (sStatus) {
				if (sStatus === "Male") {
					return "Success";
				
				} else if (sStatus === "Female"){
					return "Error";
				}
		},
		getDate: function (epoch){
			var dateEpoch = new Date(0); 
			dateEpoch.setUTCSeconds(Number(epoch));
			return dateEpoch;
		}
	};

	return Formatter;

}, /* bExport= */ true);